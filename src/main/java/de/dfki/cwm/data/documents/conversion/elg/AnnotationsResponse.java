package de.dfki.cwm.data.documents.conversion.elg;

public class AnnotationsResponse {
    public final NERAnnotations response;

    public AnnotationsResponse(NERAnnotations response) {
        this.response = response;
    }
}
