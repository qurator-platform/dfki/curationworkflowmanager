package de.dfki.cwm.communication;


public abstract class CommunicationMessage {

	public abstract byte[] getByteArray();

}
